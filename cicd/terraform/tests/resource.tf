resource "digitalocean_droplet" "clockwork-test" {
  image = "docker-20-04"
  name = "clockwork-test"
  region = "ams3"
  size = "s-1vcpu-1gb"
  private_networking = true
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]
  #connection {
  #  host = self.ipv4_address
  #  user = "root"
  #  type = "ssh"
  #  #password = "{{PASS}}"
  #  password = ".Argenteuil959595JF"
  #  timeout = "2m"
  #}
  provisioner "remote-exec" {
    inline = [
      "apt update",
      "adduser devops",
      "echo \".Argenteuil959595JF\" | passwd devops --stdin",
      "sudo usermod -aG sudo devops",
      "sudo usermod -aG docker devops"
    ]
  }
  provisioner "remote-exec" {
    inline = [
      "export PATH=$PATH:/usr/bin",
      "sysctl -p",
      "adduser --gecos '' devops",
      "echo \".Argenteuil959595JF\" | passwd devops --stdin",
      "usermod -aG admin devops",
      "usermod -aG docker devops",
      "mkdir -p /home/devops/.ssh",
      "chmod 0700 /home/devops/.ssh",
      "cp cicd/terraform/tests/authorized_keys /home/devops/.ssh",
      "chmod 0600 /home/devops/.ssh/authorized_keys",
      "chown -R devops:devops /home/devops",
      "sed -i -e '/Defaults\\s\\+env_reset/a Defaults\\texempt_group=admin/' /etc/sudoers",
      "sed -i -e 's/%admin ALL=(ALL) ALL/%admin ALL=NOPASSWD:ALL/g' /etc/sudoers",
      "visudo -cf /etc/sudoers",
      "sed -i -e 's/#PubkeyAuthentication/PubkeyAuthentication/g' /etc/ssh/sshd_config",
      "sed -i -e 's/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config",
      "/usr/sbin/sshd -t && systemctl reload sshd",
      "rm -rf /root/.ssh"
    ]
    connection {
      agent       = false
      type        = "ssh"
      private_key = "${file(var.ssh_key_path)}"
      user        = "root"
      timeout     = "5m"
    }
  }
}
#terraform show | grep "ipv4"
#output "address" {
#  value = "${digitalocean_droplet.clockwork-test.ipv4_address}"
#}
output "controller_ip_address" {
    value = "${digitalocean_droplet.clockwork-test.ipv4_address}"
}
