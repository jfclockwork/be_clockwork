# Authenticate
```
curl --request POST \
  --url http://159.65.202.145/login \
  --header 'Content-Type: application/json' \
  --data '{
	"email": "user1@gmail.com",
	"password": "password"
}'
```
# Create Project
```
curl --request POST \
  --url http://159.65.202.145/projects \
  --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2MDgwMjY0NjEsInVzZXJfaWQiOjF9.HtyR1soJTJ42yf3pxSxNqiXg3uNgU0POVmXH5OZUwpQ' \
  --header 'Content-Type: application/json' \
  --data '{
	"title": "Teste Project Prd",
	"content": "this project is for test Prd",
	"archived": false,
	"author_id": 1
}'
```
# Get Projects
```
curl --request GET \
  --url http://159.65.202.145/projects \
  --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2MDUxOTc0MDgsInVzZXJfaWQiOjF9.JwvuZ0ZN0e0R5ImQdldK8kbEUJkZmF_-N4At2qy9AbU' \
  --header 'Content-Type: application/json'
```
# Get Project
```
curl --request GET \
  --url http://159.65.202.145/projects/3 \
  --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2MDUxOTc0MDgsInVzZXJfaWQiOjF9.JwvuZ0ZN0e0R5ImQdldK8kbEUJkZmF_-N4At2qy9AbU' \
  --header 'Content-Type: application/json'
```
# Get Tasks
```
curl --request GET \
  --url http://159.65.202.145/tasks \
  --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2MDUxOTc0MDgsInVzZXJfaWQiOjF9.JwvuZ0ZN0e0R5ImQdldK8kbEUJkZmF_-N4At2qy9AbU' \
  --header 'Content-Type: application/json'
```
# Get Task
```
curl --request GET \
  --url http://159.65.202.145/tasks/1 \
  --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2MDUxOTc0MDgsInVzZXJfaWQiOjF9.JwvuZ0ZN0e0R5ImQdldK8kbEUJkZmF_-N4At2qy9AbU' \
  --header 'Content-Type: application/json'
```
# Get Clockworks
```
curl --request GET \
  --url http://159.65.202.145/clockworks \
  --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2MDUxOTc0MDgsInVzZXJfaWQiOjF9.JwvuZ0ZN0e0R5ImQdldK8kbEUJkZmF_-N4At2qy9AbU' \
  --header 'Content-Type: application/json'
```
# Get Clockwork
```
curl --request GET \
  --url http://159.65.202.145/clockworks/1 \
  --header 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2MDUxOTc0MDgsInVzZXJfaWQiOjF9.JwvuZ0ZN0e0R5ImQdldK8kbEUJkZmF_-N4At2qy9AbU' \
  --header 'Content-Type: application/json'
```