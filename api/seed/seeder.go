package seed

import (
	"log"
	"time"

	"be_clockworkv2/api/models"

	"github.com/jinzhu/gorm"
)

var users = []models.User{
	models.User{
		Nickname: "User Test1",
		Email:    "user1@gmail.com",
		Password: "password",
	},
	models.User{
		Nickname: "User Test2",
		Email:    "user2@gmail.com",
		Password: "password",
	},
}

var projects = []models.Project{
	models.Project{
		Title:   "Project 1",
		Content: "Project Msg 1",
	},
	models.Project{
		Title:   "Project 2",
		Content: "Project Msg 2",
	},
}

var tasks = []models.Task{
	models.Task{
		Title:    "Task 1",
		Priority: "1",
		Deadline: time.Now(),
		Done:     false,
	},
	models.Task{
		Title:    "Task 2",
		Priority: "2",
		Deadline: time.Now(),
		Done:     true,
	},
}

var clockworks = []models.Clockwork{
	models.Clockwork{
		Timespent: "2d",
	},
	models.Clockwork{
		Timespent: "2w 2d",
	},
}

func Load(db *gorm.DB) {

	err := db.Debug().DropTableIfExists(&models.Clockwork{}, &models.Task{}, &models.Project{}, &models.User{}).Error
	if err != nil {
		log.Fatalf("cannot drop table: %v", err)
	}
	err = db.Debug().AutoMigrate(&models.User{}, &models.Project{}, &models.Task{}, &models.Clockwork{}).Error
	if err != nil {
		log.Fatalf("cannot migrate table: %v", err)
	}

	/*
		err = db.Debug().Model(&models.Project{}).AddForeignKey("author_id", "users(id)", "cascade", "cascade").Error
		if err != nil {
			log.Fatalf("attaching foreign key error: %v", err)
		}
	*/

	for i, _ := range users {
		err = db.Debug().Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			log.Fatalf("cannot seed users table: %v", err)
		}

		projects[i].AuthorID = users[i].ID

		err = db.Debug().Model(&models.Project{}).Create(&projects[i]).Error
		if err != nil {
			log.Fatalf("cannot seed projects table: %v", err)
		}

		tasks[i].AuthorID = users[i].ID
		tasks[i].ProjectID = projects[i].ID

		err = db.Debug().Model(&models.Task{}).Create(&tasks[i]).Error
		if err != nil {
			log.Fatalf("cannot seed tasks table: %v", err)
		}

		clockworks[i].AuthorID = users[i].ID
		clockworks[i].TaskID = tasks[i].ID

		err = db.Debug().Model(&models.Clockwork{}).Create(&clockworks[i]).Error
		if err != nil {
			log.Fatalf("cannot seed clockworks table: %v", err)
		}

	}
}
