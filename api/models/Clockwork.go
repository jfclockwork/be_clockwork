package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type Clockwork struct {
	ID        uint64    `gorm:"primary_key;auto_increment" json:"id"`
	Timespent string    `gorm:"default:null" json:"timespent" example:"2w 3d 4h 30m 30s"`
	TaskID    uint64    `sql:"type:int REFERENCES tasks(id)" json:"task_id" example:"1"`
	AuthorID  uint32    `sql:"type:int REFERENCES users(id)" json:"author_id"`
	Author    User      `json:"author"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (c *Clockwork) Prepare() {
	c.ID = 0
	c.Timespent = html.EscapeString(strings.TrimSpace(c.Timespent))
	c.Author = User{}
	c.CreatedAt = time.Now()
	c.UpdatedAt = time.Now()
}

func (c *Clockwork) Validate() error {
	if c.Timespent == "" {
		return errors.New("Required Timespent")
	}
	if c.TaskID < 1 {
		return errors.New("Required Task")
	}
	if c.AuthorID < 1 {
		return errors.New("Required Author")
	}
	return nil
}

func (c *Clockwork) SaveClockwork(db *gorm.DB) (*Clockwork, error) {
	var err error
	err = db.Debug().Model(&Clockwork{}).Create(&c).Error
	if err != nil {
		return &Clockwork{}, err
	}
	if c.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", c.AuthorID).Error
		if err != nil {
			return &Clockwork{}, err
		}
		err = db.Debug().Model(&Task{}).Where("id = ?", c.TaskID).Error
		if err != nil {
			return &Clockwork{}, err
		}
	}
	return c, nil
}

func (c *Clockwork) FindAllClockworks(db *gorm.DB) (*[]Clockwork, error) {
	var err error
	clockworks := []Clockwork{}
	err = db.Debug().Model(&Clockwork{}).Limit(100).Find(&clockworks).Error
	if err != nil {
		return &[]Clockwork{}, err
	}
	if len(clockworks) > 0 {
		for i, _ := range clockworks {
			err := db.Debug().Model(&User{}).Where("id = ?", clockworks[i].AuthorID).Take(&c.Author).Error
			if err != nil {
				return &[]Clockwork{}, err
			}
			err = db.Debug().Model(&Task{}).Where("id = ?", clockworks[i].TaskID).Error
			if err != nil {
				return &[]Clockwork{}, err
			}
		}
	}
	return &clockworks, nil
}

func (c *Clockwork) FindClockworkByID(db *gorm.DB, pid uint64) (*Clockwork, error) {
	var err error
	err = db.Debug().Model(&Clockwork{}).Where("id = ?", pid).Take(&c).Error
	if err != nil {
		return &Clockwork{}, err
	}
	if c.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", c.AuthorID).Take(&c.Author).Error
		if err != nil {
			return &Clockwork{}, err
		}
		err = db.Debug().Model(&Task{}).Where("id = ?", c.TaskID).Error
		if err != nil {
			return &Clockwork{}, err
		}
	}
	return c, nil
}

func (c *Clockwork) UpdateAClockwork(db *gorm.DB) (*Clockwork, error) {

	var err error
	err = db.Debug().Model(&Clockwork{}).Where("id = ?", c.ID).Updates(Clockwork{Timespent: c.Timespent, UpdatedAt: time.Now()}).Error
	if err != nil {
		return &Clockwork{}, err
	}
	if c.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", c.AuthorID).Error
		if err != nil {
			return &Clockwork{}, err
		}
		err = db.Debug().Model(&Task{}).Where("id = ?", c.TaskID).Error
		if err != nil {
			return &Clockwork{}, err
		}
	}
	return c, nil
}

func (c *Clockwork) DeleteAClockwork(db *gorm.DB, cid uint64) (int64, error) {

	db = db.Debug().Model(&Clockwork{}).Where("id = ?", cid).Take(&Clockwork{}).Delete(&Clockwork{})

	if db.Error != nil {
		if gorm.IsRecordNotFoundError(db.Error) {
			return 0, errors.New("Clockwork not found")
		}
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
