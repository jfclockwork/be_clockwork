package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type Task struct {
	ID         uint64      `gorm:"primary_key;auto_increment" json:"id"`
	Title      string      `gorm:"size:255;not null;unique" json:"title"`
	Priority   string      `gorm:"size:255;not null;" json:"content"`
	Deadline   time.Time   `gorm:"default:CURRENT_TIMESTAMP" json:"deadline" example:"2020-11-01T21:21:46+00:00"`
	Clockworks []Clockwork `gorm:"ForeignKey:TaskID" json:"clockworks"`
	Done       bool        `json:"done"  example:"false"`
	ProjectID  uint64      `sql:"type:int REFERENCES projects(id)" json:"project_id" example:"1"`
	AuthorID   uint32      `sql:"type:int REFERENCES users(id)" json:"author_id"`
	Author     User        `json:"author"`
	CreatedAt  time.Time   `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt  time.Time   `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
}

func (t *Task) Prepare() {
	t.ID = 0
	t.Title = html.EscapeString(strings.TrimSpace(t.Title))
	t.Priority = html.EscapeString(strings.TrimSpace(t.Priority))
	t.Deadline = time.Now()
	t.Done = false
	t.Clockworks = []Clockwork{}
	t.Author = User{}
	t.CreatedAt = time.Now()
	t.UpdatedAt = time.Now()
}

func (t *Task) Validate() error {

	if t.Title == "" {
		return errors.New("Required Title")
	}
	if t.Priority == "" {
		return errors.New("Required Priority")
	}
	if t.ProjectID < 1 {
		return errors.New("Required Project")
	}
	if t.AuthorID < 1 {
		return errors.New("Required Author")
	}
	return nil
}

func (t *Task) SaveTask(db *gorm.DB) (*Task, error) {
	var err error
	err = db.Debug().Model(&Task{}).Create(&t).Error
	if err != nil {
		return &Task{}, err
	}
	if t.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", t.AuthorID).Error
		if err != nil {
			return &Task{}, err
		}
		err = db.Debug().Model(&Project{}).Where("id = ?", t.ProjectID).Error
		if err != nil {
			return &Task{}, err
		}
	}
	return t, nil
}

func (t *Task) FindAllTasks(db *gorm.DB) (*[]Task, error) {
	var err error
	tasks := []Task{}
	err = db.Debug().Model(&Task{}).Limit(100).Find(&tasks).Error
	if err != nil {
		return &[]Task{}, err
	}
	if len(tasks) > 0 {
		for i, _ := range tasks {
			err := db.Debug().Model(&User{}).Where("id = ?", tasks[i].AuthorID).Error
			if err != nil {
				return &[]Task{}, err
			}
			errP := db.Debug().Model(&Project{}).Where("id = ?", tasks[i].ProjectID).Error
			if errP != nil {
				return &[]Task{}, errP
			}
			errT := db.Debug().Model(&Clockwork{}).Where("task_id = ?", tasks[i].ID).Take(&tasks[i].Clockworks).Error
			if errT != nil {
				return &[]Task{}, errT
			}
		}
	}
	return &tasks, nil
}

func (t *Task) FindTaskByID(db *gorm.DB, pid uint64) (*Task, error) {
	var err error
	err = db.Debug().Model(&Task{}).Where("id = ?", pid).Take(&t).Error
	if err != nil {
		return &Task{}, err
	}
	if t.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", t.AuthorID).Error
		if err != nil {
			return &Task{}, err
		}
		err = db.Debug().Model(&Project{}).Where("id = ?", t.ProjectID).Error
		if err != nil {
			return &Task{}, err
		}
		err = db.Debug().Model(&Clockwork{}).Where("task_id = ?", t.ID).Take(&t.Clockworks).Error
		if err != nil {
			return &Task{}, err
		}
	}
	return t, nil
}

func (t *Task) UpdateATask(db *gorm.DB) (*Task, error) {
	var err error
	err = db.Debug().Model(&Task{}).Where("id = ?", t.ID).Updates(Task{Title: t.Title, Priority: t.Priority, Deadline: t.Deadline, Done: t.Done, UpdatedAt: time.Now()}).Error
	if err != nil {
		return &Task{}, err
	}
	if t.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", t.AuthorID).Error
		if err != nil {
			return &Task{}, err
		}
		err = db.Debug().Model(&Project{}).Where("id = ?", t.ProjectID).Error
		if err != nil {
			return &Task{}, err
		}
	}
	return t, nil
}

func (p *Task) DeleteATask(db *gorm.DB, tid uint64) (int64, error) {

	db = db.Debug().Model(&Task{}).Where("id = ?", tid).Take(&Task{}).Delete(&Task{})

	if db.Error != nil {
		if gorm.IsRecordNotFoundError(db.Error) {
			return 0, errors.New("Task not found")
		}
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
