package models

import (
	"errors"
	"html"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

type Project struct {
	ID        uint64    `gorm:"primary_key;auto_increment" json:"id"`
	Title     string    `gorm:"size:255;not null;unique" json:"title"`
	Content   string    `gorm:"size:255;not null;" json:"content"`
	Archived  bool      `json:"archived" example:"false"`
	Author    User      `json:"author"`
	AuthorID  uint32    `sql:"type:int REFERENCES users(id)" json:"author_id"`
	CreatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time `gorm:"default:CURRENT_TIMESTAMP" json:"updated_at"`
	Tasks     []Task    `gorm:"ForeignKey:ProjectID" json:"tasks"`
}

func (p *Project) Prepare() {
	p.ID = 0
	p.Title = html.EscapeString(strings.TrimSpace(p.Title))
	p.Content = html.EscapeString(strings.TrimSpace(p.Content))
	p.Author = User{}
	p.CreatedAt = time.Now()
	p.UpdatedAt = time.Now()
	p.Archived = false
	//p.Tasks = []Task{}
}

func (p *Project) Validate() error {

	if p.Title == "" {
		return errors.New("Required Title")
	}
	if p.Content == "" {
		return errors.New("Required Content")
	}
	if p.AuthorID < 1 {
		return errors.New("Required Author")
	}
	return nil
}

func (p *Project) SaveProject(db *gorm.DB) (*Project, error) {
	var err error
	err = db.Debug().Model(&Project{}).Create(&p).Error
	if err != nil {
		return &Project{}, err
	}
	if p.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", p.AuthorID).Take(&p.Author).Error
		if err != nil {
			return &Project{}, err
		}
	}
	return p, nil
}

func (p *Project) FindAllProjects(db *gorm.DB) (*[]Project, error) {
	var err error
	projects := []Project{}
	err = db.Debug().Model(&Project{}).Limit(100).Find(&projects).Error
	if err != nil {
		return &[]Project{}, err
	}
	if len(projects) > 0 {
		for i, _ := range projects {
			err := db.Debug().Model(&User{}).Where("id = ?", projects[i].AuthorID).Take(&projects[i].Author).Error
			if err != nil {
				return &[]Project{}, err
			}
			errP := db.Debug().Model(&Task{}).Where("project_id = ?", projects[i].ID).Take(&projects[i].Tasks).Error
			if errP != nil {
				return &[]Project{}, errP
			}
			if len(projects[i].Tasks) > 0 {
				for t, _ := range projects[i].Tasks {
					errC := db.Debug().Model(&Clockwork{}).Where("task_id = ?", projects[i].Tasks[t].ID).Take(&projects[i].Tasks[t].Clockworks).Error
					if errC != nil {
						return &[]Project{}, errC
					}
				}
			}
		}
	}
	return &projects, nil
}

func (p *Project) FindProjectByID(db *gorm.DB, pid uint64) (*Project, error) {
	var err error
	err = db.Debug().Model(&Project{}).Where("id = ?", pid).Take(&p).Error
	if err != nil {
		return &Project{}, err
	}
	if p.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", p.AuthorID).Take(&p.Author).Error
		if err != nil {
			return &Project{}, err
		}
		err := db.Debug().Model(&Task{}).Where("project_id = ?", p.ID).Take(&p.Tasks).Error
		if err != nil {
			return &Project{}, err
		}
		if len(p.Tasks) > 0 {
			for t, _ := range p.Tasks {
				errC := db.Debug().Model(&Clockwork{}).Where("task_id = ?", p.Tasks[t].ID).Take(&p.Tasks[t].Clockworks).Error
				if errC != nil {
					return &Project{}, err
				}
			}
		}
	}
	return p, nil
}

func (p *Project) UpdateAProject(db *gorm.DB) (*Project, error) {

	var err error
	err = db.Debug().Model(&Project{}).Where("id = ?", p.ID).Updates(Project{Title: p.Title, Content: p.Content, UpdatedAt: time.Now()}).Error
	if err != nil {
		return &Project{}, err
	}
	if p.ID != 0 {
		err = db.Debug().Model(&User{}).Where("id = ?", p.AuthorID).Take(&p.Author).Error
		if err != nil {
			return &Project{}, err
		}
	}
	return p, nil
}

func (p *Project) DeleteAProject(db *gorm.DB, pid uint64, uid uint32) (int64, error) {

	db = db.Debug().Model(&Project{}).Where("id = ? and author_id = ?", pid, uid).Take(&Project{}).Delete(&Project{})

	if db.Error != nil {
		if gorm.IsRecordNotFoundError(db.Error) {
			return 0, errors.New("Project not found")
		}
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
