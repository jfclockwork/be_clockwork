package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"be_clockworkv2/api/auth"
	"be_clockworkv2/api/models"
	"be_clockworkv2/api/responses"
	"be_clockworkv2/api/utils/formaterror"

	"github.com/gorilla/mux"
)

func (server *Server) CreateClockwork(w http.ResponseWriter, r *http.Request) {

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	clockwork := models.Clockwork{}
	err = json.Unmarshal(body, &clockwork)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	clockwork.Prepare()
	err = clockwork.Validate()
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}
	uid, err := auth.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}
	if uid != clockwork.AuthorID {
		responses.ERROR(w, http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}
	clockworkCreated, err := clockwork.SaveClockwork(server.DB)
	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	w.Header().Set("Location", fmt.Sprintf("%s%s/%d", r.Host, r.URL.Path, clockworkCreated.ID))
	responses.JSON(w, http.StatusCreated, clockworkCreated)
}

func (server *Server) GetClockworks(w http.ResponseWriter, r *http.Request) {

	clockwork := models.Clockwork{}

	clockworks, err := clockwork.FindAllClockworks(server.DB)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	responses.JSON(w, http.StatusOK, clockworks)
}

func (server *Server) GetClockwork(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	cid, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	clockwork := models.Clockwork{}

	clockworkReceived, err := clockwork.FindClockworkByID(server.DB, cid)
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	responses.JSON(w, http.StatusOK, clockworkReceived)
}

func (server *Server) UpdateClockwork(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)

	// Check if the clockwork id is valid
	cid, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	//CHeck if the auth token is valid and  get the user id from it
	uid, err := auth.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	// Check if the clockwork exist
	clockwork := models.Clockwork{}
	err = server.DB.Debug().Model(models.Clockwork{}).Where("id = ?", cid).Take(&clockwork).Error
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, errors.New("Clockwork not found"))
		return
	}

	// If a user attempt to update a clockwork not belonging to him
	if uid != clockwork.AuthorID {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}
	// Read the data clockworked
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Start processing the request data
	clockworkUpdate := models.Clockwork{}
	err = json.Unmarshal(body, &clockworkUpdate)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	//Also check if the request user id is equal to the one gotten from token
	if uid != clockworkUpdate.AuthorID {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	clockworkUpdate.Prepare()
	err = clockworkUpdate.Validate()
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	clockworkUpdate.ID = clockwork.ID //this is important to tell the model the clockwork id to update, the other update field are set above

	clockworkUpdated, err := clockworkUpdate.UpdateAClockwork(server.DB)

	if err != nil {
		formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, formattedError)
		return
	}
	responses.JSON(w, http.StatusOK, clockworkUpdated)
}

func (server *Server) DeleteClockwork(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)

	// Is a valid clockwork id given to us?
	cid, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}

	// Is this user authenticated?
	uid, err := auth.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	// Check if the clockwork exist
	clockwork := models.Clockwork{}
	err = server.DB.Debug().Model(models.Clockwork{}).Where("id = ?", cid).Take(&clockwork).Error
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, errors.New("Unauthorized"))
		return
	}

	// Is the authenticated user, the owner of this clockwork?
	if uid != clockwork.AuthorID {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}
	_, err = clockwork.DeleteAClockwork(server.DB, cid)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	w.Header().Set("Entity", fmt.Sprintf("%d", cid))
	responses.JSON(w, http.StatusNoContent, "")
}
