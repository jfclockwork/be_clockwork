package controllers

import "be_clockworkv2/api/middlewares"

func (s *Server) initializeRoutes() {

	// Home Route
	s.Router.HandleFunc("/", middlewares.SetMiddlewareJSON(s.Home)).Methods("GET")

	// Login Route
	s.Router.HandleFunc("/login", middlewares.SetMiddlewareJSON(s.Login)).Methods("POST")

	//Users routes
	s.Router.HandleFunc("/users", middlewares.SetMiddlewareJSON(s.CreateUser)).Methods("POST")
	s.Router.HandleFunc("/users", middlewares.SetMiddlewareJSON(s.GetUsers)).Methods("GET")
	s.Router.HandleFunc("/users/{id}", middlewares.SetMiddlewareJSON(s.GetUser)).Methods("GET")
	s.Router.HandleFunc("/users/{id}", middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.UpdateUser))).Methods("PUT")
	s.Router.HandleFunc("/users/{id}", middlewares.SetMiddlewareAuthentication(s.DeleteUser)).Methods("DELETE")

	//Projects routes
	s.Router.HandleFunc("/projects", middlewares.SetMiddlewareJSON(s.CreateProject)).Methods("POST")
	s.Router.HandleFunc("/projects", middlewares.SetMiddlewareJSON(s.GetProjects)).Methods("GET")
	s.Router.HandleFunc("/projects/{id}", middlewares.SetMiddlewareJSON(s.GetProject)).Methods("GET")
	s.Router.HandleFunc("/projects/{id}", middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.UpdateProject))).Methods("PUT")
	s.Router.HandleFunc("/projects/{id}", middlewares.SetMiddlewareAuthentication(s.DeleteProject)).Methods("DELETE")

	//Tasks routes
	s.Router.HandleFunc("/tasks", middlewares.SetMiddlewareJSON(s.CreateTask)).Methods("POST")
	s.Router.HandleFunc("/tasks", middlewares.SetMiddlewareJSON(s.GetTasks)).Methods("GET")
	s.Router.HandleFunc("/tasks/{id}", middlewares.SetMiddlewareJSON(s.GetTask)).Methods("GET")
	s.Router.HandleFunc("/tasks/{id}", middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.UpdateTask))).Methods("PUT")
	s.Router.HandleFunc("/tasks/{id}", middlewares.SetMiddlewareAuthentication(s.DeleteTask)).Methods("DELETE")

	//Clockwork routes
	s.Router.HandleFunc("/clockworks", middlewares.SetMiddlewareJSON(s.CreateClockwork)).Methods("POST")
	s.Router.HandleFunc("/clockworks", middlewares.SetMiddlewareJSON(s.GetClockworks)).Methods("GET")
	s.Router.HandleFunc("/clockworks/{id}", middlewares.SetMiddlewareJSON(s.GetClockwork)).Methods("GET")
	s.Router.HandleFunc("/clockworks/{id}", middlewares.SetMiddlewareJSON(middlewares.SetMiddlewareAuthentication(s.UpdateClockwork))).Methods("PUT")
	s.Router.HandleFunc("/clockworks/{id}", middlewares.SetMiddlewareAuthentication(s.DeleteClockwork)).Methods("DELETE")

}
