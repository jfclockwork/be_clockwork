package controllers

import (
	"net/http"

	"be_clockworkv2/api/responses"
)

func (server *Server) Home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Api For Clockwork App 1.0.0")
}
