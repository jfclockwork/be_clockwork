package controllertests

import (
	"fmt"
	"log"
	"os"
	"testing"

	"be_clockworkv2/api/controllers"
	"be_clockworkv2/api/models"

	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
)

var server = controllers.Server{}
var userInstance = models.User{}
var projectInstance = models.Project{}

func TestMain(m *testing.M) {
	err := godotenv.Load(os.ExpandEnv("../../.env"))
	if err != nil {
		log.Fatalf("Error getting env %v\n", err)
	}
	Database()

	os.Exit(m.Run())

}

func Database() {

	var err error

	TestDbDriver := os.Getenv("TestDbDriver")

	if TestDbDriver == "mysql" {
		DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", os.Getenv("TestDbUser"), os.Getenv("TestDbPassword"), os.Getenv("TestDbHost"), os.Getenv("TestDbPort"), os.Getenv("TestDbName"))
		server.DB, err = gorm.Open(TestDbDriver, DBURL)
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", TestDbDriver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database\n", TestDbDriver)
		}
	}
	if TestDbDriver == "postgres" {
		DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", os.Getenv("TestDbHost"), os.Getenv("TestDbPort"), os.Getenv("TestDbUser"), os.Getenv("TestDbName"), os.Getenv("TestDbPassword"))
		server.DB, err = gorm.Open(TestDbDriver, DBURL)
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", TestDbDriver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database\n", TestDbDriver)
		}
	}
	if TestDbDriver == "sqlite3" {
		//DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", DbHost, DbPort, DbUser, DbName, DbPassword)
		testDbName := os.Getenv("TestDbName")
		server.DB, err = gorm.Open(TestDbDriver, testDbName)
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", TestDbDriver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database\n", TestDbDriver)
		}
		server.DB.Exec("PRAGMA foreign_keys = ON")
	}
}

func refreshAll() error {
	err := server.DB.Debug().DropTableIfExists(&models.Clockwork{}).Error
	if err != nil {
		return err
	}
	err = server.DB.Debug().DropTableIfExists(&models.Task{}).Error
	if err != nil {
		return err
	}
	err = server.DB.Debug().DropTableIfExists(&models.Project{}).Error
	if err != nil {
		return err
	}
	err = server.DB.Debug().DropTableIfExists(&models.User{}).Error
	if err != nil {
		return err
	}
	err = server.DB.Debug().AutoMigrate(&models.User{}).Error
	if err != nil {
		return err
	}
	err = server.DB.Debug().AutoMigrate(&models.Project{}).Error
	if err != nil {
		return err
	}
	err = server.DB.Debug().AutoMigrate(&models.Task{}).Error
	if err != nil {
		return err
	}
	err = server.DB.Debug().AutoMigrate(&models.Clockwork{}).Error
	if err != nil {
		return err
	}
	log.Printf("Successfully refreshed all tables")
	return nil
}

func refreshUserTable() error {
	err := server.DB.Debug().DropTableIfExists(&models.User{}).Error
	if err != nil {
		return err
	}
	err = server.DB.AutoMigrate(&models.User{}).Error
	if err != nil {
		return err
	}

	log.Printf("Successfully refreshed table(s)")
	return nil
}

func seedOneUser() (models.User, error) {

	/*err := refreshUserTable()
	if err != nil {
		log.Fatal(err)
	}
	*/

	user := models.User{
		Nickname: "user",
		Email:    "user@gmail.com",
		Password: "password",
	}

	err := server.DB.Model(&models.User{}).Create(&user).Error
	if err != nil {
		return models.User{}, err
	}
	return user, nil
}

func seedUsers() ([]models.User, error) {

	var err error
	if err != nil {
		return nil, err
	}
	users := []models.User{
		models.User{
			Nickname: "User 1",
			Email:    "user1@gmail.com",
			Password: "password",
		},
		models.User{
			Nickname: "User 2",
			Email:    "user2@gmail.com",
			Password: "password",
		},
	}
	for i, _ := range users {
		err := server.DB.Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			return []models.User{}, err
		}
	}
	return users, nil
}

func refreshUserAndProjectTable() error {

	err := server.DB.DropTableIfExists(&models.Project{}, &models.User{}).Error
	if err != nil {
		return err
	}
	err = server.DB.AutoMigrate(&models.User{}, &models.Project{}).Error
	if err != nil {
		return err
	}
	log.Printf("Successfully refreshed tables")
	return nil
}

func seedOneUserAndOneProject() (models.Project, error) {

	/*err := refreshUserAndProjectTable()
	if err != nil {
		return models.Project{}, err
	}*/
	user := models.User{
		Nickname: "User",
		Email:    "user@gmail.com",
		Password: "password",
	}
	err := server.DB.Model(&models.User{}).Create(&user).Error
	if err != nil {
		return models.Project{}, err
	}
	project := models.Project{
		Title:    "This is the title test",
		Content:  "This is the content test",
		AuthorID: user.ID,
	}
	err = server.DB.Model(&models.Project{}).Create(&project).Error
	if err != nil {
		return models.Project{}, err
	}
	return project, nil
}

func seedUsersAndProjects() ([]models.User, []models.Project, error) {

	var err error

	if err != nil {
		return []models.User{}, []models.Project{}, err
	}
	var users = []models.User{
		models.User{
			Nickname: "User 4",
			Email:    "user4@gmail.com",
			Password: "password",
		},
		models.User{
			Nickname: "User 5",
			Email:    "user5@gmail.com",
			Password: "password",
		},
	}
	var projects = []models.Project{
		models.Project{
			Title:   "Title 1",
			Content: "Msg 1",
		},
		models.Project{
			Title:   "Title 2",
			Content: "Msg 2",
		},
	}

	for i, _ := range users {
		err = server.DB.Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			log.Fatalf("cannot seed users table: %v", err)
		}
		projects[i].AuthorID = users[i].ID

		err = server.DB.Model(&models.Project{}).Create(&projects[i]).Error
		if err != nil {
			log.Fatalf("cannot seed projects table: %v", err)
		}
	}
	return users, projects, nil
}
