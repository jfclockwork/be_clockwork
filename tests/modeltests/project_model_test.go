package modeltests

import (
	"log"
	"testing"

	"be_clockworkv2/api/models"

	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gopkg.in/go-playground/assert.v1"
)

func TestFindAllProjects(t *testing.T) {

	err := refreshAllTables()
	if err != nil {
		log.Fatal(err)
	}
	_, _, err = seedUsersAndProjects()
	if err != nil {
		log.Fatalf("Error seeding user and project table %v\n", err)
	}
	projects, err := projectInstance.FindAllProjects(server.DB)
	if err != nil {
		t.Errorf("this is the error getting the projects: %v\n", err)
		return
	}
	assert.Equal(t, len(*projects), 2)
}

func TestSaveProject(t *testing.T) {

	err := refreshAllTables()
	if err != nil {
		log.Fatal(err)
	}
	/*err := refreshUserAndProjectTable()
	if err != nil {
		log.Fatalf("Error user and project refreshing table %v\n", err)
	}*/
	user, err := seedOneUser()
	if err != nil {
		log.Fatalf("Cannot seed user %v\n", err)
	}

	newProject := models.Project{
		ID:       1,
		Title:    "This is the title",
		Content:  "This is the content",
		AuthorID: user.ID,
	}
	savedProject, err := newProject.SaveProject(server.DB)
	if err != nil {
		t.Errorf("this is the error getting the project: %v\n", err)
		return
	}
	assert.Equal(t, newProject.ID, savedProject.ID)
	assert.Equal(t, newProject.Title, savedProject.Title)
	assert.Equal(t, newProject.Content, savedProject.Content)
	assert.Equal(t, newProject.AuthorID, savedProject.AuthorID)

}

func TestGetProjectByID(t *testing.T) {
	err := refreshAllTables()
	if err != nil {
		log.Fatal(err)
	}
	/*
		err := refreshUserAndProjectTable()
		if err != nil {
			log.Fatalf("Error refreshing user and project table: %v\n", err)
		}*/
	project, err := seedOneUserAndOneProject()
	if err != nil {
		log.Fatalf("Error Seeding User and project table")
	}
	foundProject, err := projectInstance.FindProjectByID(server.DB, project.ID)
	if err != nil {
		t.Errorf("this is the error getting one user: %v\n", err)
		return
	}
	assert.Equal(t, foundProject.ID, project.ID)
	assert.Equal(t, foundProject.Title, project.Title)
	assert.Equal(t, foundProject.Content, project.Content)
}

func TestUpdateAProject(t *testing.T) {
	err := refreshAllTables()
	if err != nil {
		log.Fatal(err)
	}
	/*
		err := refreshUserAndProjectTable()
		if err != nil {
			log.Fatalf("Error refreshing user and project table: %v\n", err)
		}*/
	project, err := seedOneUserAndOneProject()
	if err != nil {
		log.Fatalf("Error Seeding table")
	}
	projectUpdate := models.Project{
		ID:       1,
		Title:    "modiUpdate",
		Content:  "modiupdate@gmail.com",
		AuthorID: project.AuthorID,
	}
	updatedProject, err := projectUpdate.UpdateAProject(server.DB)
	if err != nil {
		t.Errorf("this is the error updating the user: %v\n", err)
		return
	}
	assert.Equal(t, updatedProject.ID, projectUpdate.ID)
	assert.Equal(t, updatedProject.Title, projectUpdate.Title)
	assert.Equal(t, updatedProject.Content, projectUpdate.Content)
	assert.Equal(t, updatedProject.AuthorID, projectUpdate.AuthorID)
}

func TestDeleteAProject(t *testing.T) {
	err := refreshAllTables()
	if err != nil {
		log.Fatal(err)
	}
	/*
		err := refreshUserAndProjectTable()
		if err != nil {
			log.Fatalf("Error refreshing user and project table: %v\n", err)
		}*/
	project, err := seedOneUserAndOneProject()
	if err != nil {
		log.Fatalf("Error Seeding tables")
	}
	isDeleted, err := projectInstance.DeleteAProject(server.DB, project.ID, project.AuthorID)
	if err != nil {
		t.Errorf("this is the error deleting the user: %v\n", err)
		return
	}
	//one shows that the record has been deleted or:
	// assert.Equal(t, int(isDeleted), 1)

	//Can be done this way too
	assert.Equal(t, isDeleted, int64(1))
}
