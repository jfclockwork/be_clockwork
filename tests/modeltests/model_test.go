package modeltests

import (
	"fmt"
	"log"
	"os"
	"testing"

	"be_clockworkv2/api/controllers"
	"be_clockworkv2/api/models"

	"github.com/jinzhu/gorm"
	"github.com/joho/godotenv"
)

var server = controllers.Server{}
var userInstance = models.User{}
var projectInstance = models.Project{}

func TestMain(m *testing.M) {
	var err error
	err = godotenv.Load(os.ExpandEnv("../../.env"))
	if err != nil {
		log.Fatalf("Error getting env %v\n", err)
	}
	Database()

	log.Printf("Before calling m.Run() !!!")
	ret := m.Run()
	log.Printf("After calling m.Run() !!!")
	//os.Exit(m.Run())
	os.Exit(ret)
}

func Database() {

	var err error

	TestDbDriver := os.Getenv("TestDbDriver")

	if TestDbDriver == "mysql" {
		DBURL := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local", os.Getenv("TestDbUser"), os.Getenv("TestDbPassword"), os.Getenv("TestDbHost"), os.Getenv("TestDbPort"), os.Getenv("TestDbName"))
		server.DB, err = gorm.Open(TestDbDriver, DBURL)
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", TestDbDriver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database\n", TestDbDriver)
		}
	}
	if TestDbDriver == "postgres" {
		DBURL := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", os.Getenv("TestDbHost"), os.Getenv("TestDbPort"), os.Getenv("TestDbUser"), os.Getenv("TestDbName"), os.Getenv("TestDbPassword"))
		server.DB, err = gorm.Open(TestDbDriver, DBURL)
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", TestDbDriver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database\n", TestDbDriver)
		}
	}
	if TestDbDriver == "sqlite3" {
		testDbName := os.Getenv("TestDbName")
		server.DB, err = gorm.Open(TestDbDriver, testDbName)
		if err != nil {
			fmt.Printf("Cannot connect to %s database\n", TestDbDriver)
			log.Fatal("This is the error:", err)
		} else {
			fmt.Printf("We are connected to the %s database\n", TestDbDriver)
		}
		server.DB.Exec("PRAGMA foreign_keys = ON")
	}

}

func refreshAllTables() error {
	//server.DB.Exec("SET foreign_key_checks=0")
	log.Printf("Drop all tables")

	err := server.DB.DropTableIfExists(&models.Clockwork{}).Error
	if err != nil {
		return err
	}
	err = server.DB.DropTableIfExists(&models.Task{}).Error
	if err != nil {
		return err
	}
	err = server.DB.DropTableIfExists(&models.Project{}).Error
	if err != nil {
		return err
	}
	err = server.DB.DropTableIfExists(&models.User{}).Error
	if err != nil {
		return err
	}
	log.Printf("Recreate all tables")
	//server.DB.Exec("SET foreign_key_checks=1")
	log.Printf("Recreate table users")
	err = server.DB.AutoMigrate(&models.User{}).Error
	if err != nil {
		return err
	}
	log.Printf("Recreate table projects")
	err = server.DB.AutoMigrate(&models.Project{}).Error
	if err != nil {
		return err
	}
	log.Printf("Recreate table tasks")
	err = server.DB.AutoMigrate(&models.Task{}).Error
	if err != nil {
		return err
	}
	log.Printf("Recreate table clockworks")
	err = server.DB.AutoMigrate(&models.Clockwork{}).Error
	if err != nil {
		return err
	}
	log.Printf("Successfully refreshed all tables")
	return nil
}

func refreshUserTable() error {
	//server.DB.Exec("SET foreign_key_checks=0")
	err := server.DB.Debug().DropTableIfExists(&models.Clockwork{}, &models.Task{}, &models.Project{}, &models.User{}).Error
	if err != nil {
		return err
	}
	//server.DB.Exec("SET foreign_key_checks=1")
	err = server.DB.Debug().AutoMigrate(&models.User{}).Error
	if err != nil {
		return err
	}
	log.Printf("Successfully refreshed table")
	log.Printf("refreshUserTable routine OK !!!")
	return nil
}

func seedOneUser() (models.User, error) {

	//_ = refreshUserTable()

	user := models.User{
		Nickname: "Test",
		Email:    "test@gmail.com",
		Password: "password",
	}

	err := server.DB.Debug().Model(&models.User{}).Create(&user).Error
	if err != nil {
		log.Fatalf("cannot seed users table: %v", err)
	}

	log.Printf("seedOneUser routine OK !!!")
	return user, nil
}

func seedUsers() error {

	users := []models.User{
		models.User{
			Nickname: "test1",
			Email:    "test1@gmail.com",
			Password: "password",
		},
		models.User{
			Nickname: "test2",
			Email:    "teste2@gmail.com",
			Password: "password",
		},
	}

	for i := range users {
		err := server.DB.Debug().Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			return err
		}
	}

	log.Printf("seedUsers routine OK !!!")
	return nil
}

func refreshUserAndProjectTable() error {

	//server.DB.Exec("SET foreign_key_checks=0")
	// NOTE: when deleting first delete Project as Project is depending on User table

	err := server.DB.Debug().DropTableIfExists(&models.Clockwork{}, &models.Task{}, &models.Project{}, &models.User{}).Error
	if err != nil {
		return err
	}
	//server.DB.Exec("SET foreign_key_checks=1")
	err = server.DB.Debug().AutoMigrate(&models.User{}, &models.Project{}, &models.Task{}, &models.Clockwork{}).Error
	if err != nil {
		return err
	}
	log.Printf("Successfully refreshed tables")
	log.Printf("refreshUserAndProjectTable routine OK !!!")
	return nil
}

func seedOneUserAndOneProject() (models.Project, error) {

	/*err := refreshUserAndProjectTable()
	if err != nil {
		return models.Project{}, err
	}*/
	user := models.User{
		Nickname: "Test Name",
		Email:    "test@gmail.com",
		Password: "password",
	}
	err := server.DB.Debug().Model(&models.User{}).Create(&user).Error
	if err != nil {
		return models.Project{}, err
	}
	project := models.Project{
		Title:    "This is the title test",
		Content:  "This is the content test",
		AuthorID: user.ID,
	}
	err = server.DB.Debug().Model(&models.Project{}).Create(&project).Error
	if err != nil {
		return models.Project{}, err
	}

	log.Printf("seedOneUserAndOneProject routine OK !!!")
	return project, nil
}

func seedUsersAndProjects() ([]models.User, []models.Project, error) {

	var err error

	if err != nil {
		return []models.User{}, []models.Project{}, err
	}
	var users = []models.User{
		models.User{
			Nickname: "Test 2",
			Email:    "test2@gmail.com",
			Password: "password",
		},
		models.User{
			Nickname: "Test3",
			Email:    "test3@gmail.com",
			Password: "password",
		},
	}
	var projects = []models.Project{
		models.Project{
			Title:   "Title 1",
			Content: "content project 1",
		},
		models.Project{
			Title:   "Title 2",
			Content: "content project 2",
		},
	}

	for i := range users {
		err = server.DB.Debug().Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			log.Fatalf("cannot seed users table: %v", err)
		}
		projects[i].AuthorID = users[i].ID

		err = server.DB.Debug().Model(&models.Project{}).Create(&projects[i]).Error
		if err != nil {
			log.Fatalf("cannot seed projects table: %v", err)
		}
	}
	log.Printf("seedUsersAndProjects routine OK !!!")
	return users, projects, nil
}
